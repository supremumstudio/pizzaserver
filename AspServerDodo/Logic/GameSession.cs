﻿using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DodoDataModel;

namespace AspServerDodo.Logic
{
    public class GameSession
    {

        public Player player1 { get; set; }
        public Player player2 { get; set; }

        public GameSession(Room room)
        {
            player1 = new Player(room.Users[0]);
            player2 = new Player(room.Users[1]);
        }

        public void SetReady(string connectionID, bool state)
        {
            if(Program.UnityClients[player1.user].ConnectionID == connectionID)
            {
                player1.IsReadyToGame = state;
            }
            if(Program.UnityClients[player2.user].ConnectionID == connectionID)
            {
                player2.IsReadyToGame = state;
            }

            if(IsAllPlayersReady())
            {
                NotifyReadyClients();
            }
            else
            {
                Console.WriteLine("Not Ready!");
            }
        }

        public void SetStartTimeElapsed(string connectionID)
        {
            if (Program.UnityClients[player1.user].ConnectionID == connectionID)
            {
                player1.IsStartTimeEllapsed = true;
            }
            if (Program.UnityClients[player2.user].ConnectionID == connectionID)
            {
                player2.IsStartTimeEllapsed = true;
            }

            if(IsStartupTimeElapsed())
            {
                NotifyStartupTimeElapsed();
            }
            else
            {
                Console.WriteLine("Time elapsed only on one client!");
            }

        }
        
        //Уведомить клиентов, что оба игрока подтвердили готовность
        private void NotifyReadyClients()
        {
            Program.UnityClients[player1.user].ClientProxy.SendCoreAsync("OnAllClientsReady", new object[] { });
            Program.UnityClients[player2.user].ClientProxy.SendCoreAsync("OnAllClientsReady", new object[] { });
        }

        //Уведомить клиентов, что у обоих иргоков отыграл стартовый счетчик
        private void NotifyStartupTimeElapsed()
        {
            Program.UnityClients[player1.user].ClientProxy.SendCoreAsync("OnStartupTimeElapsed", new object[] { });
            Program.UnityClients[player2.user].ClientProxy.SendCoreAsync("OnStartupTimeElapsed", new object[] { });
            StartGame(1000, 200);
        }
        
        //Подтвердили ли оба игрока готовность
        public bool IsAllPlayersReady()
        {
            return player1.IsReadyToGame && player2.IsReadyToGame;
        }

        //Закончился ли стартовый отсчет у обоих игроков
        public bool IsStartupTimeElapsed()
        {
            return player1.IsStartTimeEllapsed && player2.IsStartTimeEllapsed;
        }


        private Timer serverTimer;

        // Запуск таймера на сервере
        public void StartGame(int dueToStart, int period)
        {
            if(serverTimer == null)
            {
                serverTimer = new Timer(SendGameDataToPlayers, null, dueToStart, period);
            }
            
        }

        private int Counter;

        public void SendGameDataToPlayers(object state)
        {
            Counter++;
            Helper.Log(Counter.ToString());
            TransportData player1Data = new TransportData() { PercentCoordinate = Counter, PercentSpeed = 1 };
            TransportData player2Data = new TransportData() { PercentCoordinate = Counter, PercentSpeed = 1 };
            Program.UnityClients[player1.user].ClientProxy.SendCoreAsync("OnCoordinateChanged", new object[] { Newtonsoft.Json.JsonConvert.SerializeObject(new TransportData[] {player1Data, player2Data })});
            Program.UnityClients[player2.user].ClientProxy.SendCoreAsync("OnCoordinateChanged", new object[] { Newtonsoft.Json.JsonConvert.SerializeObject(new TransportData[] { player1Data, player2Data })});
        }

    }
}
