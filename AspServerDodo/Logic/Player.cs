﻿using DodoDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspServerDodo.Logic
{
    public class Player
    {

        public bool IsReadyToGame;
        public bool IsStartTimeEllapsed;

        public User user { get; }

        public Player(User user)
        {
            this.user = user;
        }
    }
}
