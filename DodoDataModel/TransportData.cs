﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DodoDataModel
{
    public struct TransportData
    {
        public double PercentCoordinate;
        public double PercentSpeed;
    }
}
